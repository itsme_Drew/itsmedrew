import React from 'react';
import Main from './routes'
import "@vetixy/circular-std";
import './App.scss';

function App() {
  return (
    <div className="App">
      <Main />
    </div>
  );
}

export default App;
