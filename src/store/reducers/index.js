import { combineReducers } from 'redux';
import pages from './page_reducer';

const rootReducer = combineReducers({
  pageList: pages
});

export default rootReducer;
