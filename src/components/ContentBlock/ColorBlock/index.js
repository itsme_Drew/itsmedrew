import React, { Component } from 'react'
import PropTypes from 'prop-types'
import classnames from 'classnames'

class ColorBlock extends Component {
  renderClick(e) {
    e.preventDefault();

    this.props.onClick();
  }

  render() {
    const {children, title, color, className, isDecor, isCenter, isHero, isSwiggly} = this.props

    const headingClasses = {
      'swiggly': isSwiggly,
      'text--center': isCenter,
      'color-block__heading--decor': isDecor,
      'color-block__heading': true
    }

    return (
      <div className={classnames('color-block', className, color, {'hero': isHero})}>
        <header className={classnames('color-block__header')}>
          <h2 className={classnames(headingClasses)}>{title}</h2>
        </header>
        {children}
      </div>
    )
  }
}

ColorBlock.propTypes = {
  isHero: PropTypes.bool,
  isDecor: PropTypes.bool,
  isCenter: PropTypes.bool,
  isSwiggly: PropTypes.bool,
  title: PropTypes.string.isRequired,
  color: PropTypes.string.isRequired,
  className: PropTypes.string
}

ColorBlock.defaultProps = {
  isDecor: false,
  isHero: false,
  isCenter: false,
  isSwiggly: true
}

export default ColorBlock;
