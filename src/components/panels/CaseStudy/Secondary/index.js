import React, { Component } from 'react'
import PropTypes from 'prop-types'
import classnames from 'classnames'
import Logo from '../../../logos'
import Button from '../../../Buttons/Primary'

class CaseStudyPanel extends Component {
  render() {
    const { image, title, text, logo, link, color } = this.props
    let logoEl

    if (logo) { logoEl = logo }

    return (
      <div className={classnames("cs-panel-secondary", color)}>
        <header className="cs-panel-secondary__header">
          <div className="cs-panel-secondary__logo"><Logo name={logoEl} /></div>
          <span className="cs-panel-secondary__title">{title}</span>
          <p className="cs-panel-secondary__text">{text}</p>
          <Button type="solid" to={ link }>View Project</Button>
        </header>
        <img className="cs-panel-secondary__image" src={image} alt={title}/>
      </div>
    )
  }
}

CaseStudyPanel.propTypes = {
  image: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
  logo: PropTypes.string.isRequired,
  link: PropTypes.string.isRequired
}

export default CaseStudyPanel
