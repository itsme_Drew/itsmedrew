import React, { Component } from 'react'
import PropTypes from 'prop-types'
import classnames from 'classnames'
import Logo from '../../../logos'

class CaseStudyPanel extends Component {
  render() {
    const { image, title, text, logo, link, color, year, backgroundImage } = this.props
    let logoEl

    if (logo) { logoEl = logo }

    const headerStyle = backgroundImage ? { backgroundImage: `url(${backgroundImage})` } : {};

    return (
      <a className="cs-panel-primary" href={link}>
        <header className={classnames("cs-panel-primary__header", color)} style={headerStyle}>
          <div className="cs-panel-primary__logo"><Logo name={logoEl} /></div>
          <img className="cs-panel-primary__image" src={image} alt={title} />
        </header>
        <div className="cs-panel-primary__content">
          <span className="cs-panel-primary__year">{year}</span>
          <h2 className="cs-panel-primary__title">{title}</h2>
          <p className="cs-panel-primary__text">{text}</p>
          <span className="cs-panel-primary__cta">View Case Study →</span>
        </div>
      </a>
    )
  }
}

CaseStudyPanel.propTypes = {
  image: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  logo: PropTypes.string.isRequired,
  link: PropTypes.string.isRequired,
  backgroundImage: PropTypes.string
}

export default CaseStudyPanel
