import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Container, Row, Col } from 'react-grid-system'
import classnames from 'classnames'

class PrimaryPageHeader extends Component {
  renderContent = () => {
    const { featuredImage, updated, title, subtitle, prototypeId, figmaLink } = this.props

    let updatedEl
    let prototypeIdEl
    let featuredImageEl
    let detailEl
    let figmaLinkEl

    if (updated) {
      updatedEl = <h6 className="primary-ph__subtitle">Updated: {updated}</h6>
    }

    if (prototypeId) {
      prototypeIdEl = <a href={prototypeId} className="primary-ph__btn btn btn-primary">Jump to Prototypes</a>
    }

    if (figmaLink) {
      figmaLinkEl = <a href={figmaLink} className="primary-ph__btn btn btn-primary">View Figma Prototype</a>
    }

    detailEl = <div>
                  <h1 className="primary-ph__title">{ title }</h1>
                  <h5 className="primary-ph__subtitle">{ subtitle }</h5>
                  { updatedEl }
                  { prototypeIdEl }
                  { figmaLinkEl }
                </div>

    if (featuredImage) {
      featuredImageEl = <img src={ featuredImage } alt={ title }/>
      return (
        <Row align="center">
          <Col md={3} className="primary-ph__preview">
            { featuredImageEl }
          </Col>
          <Col md={8} push={{md: 1}} className="primary-ph__detail">
            { detailEl }
          </Col>
        </Row>
      )
    } else {
      return (
        <Row align="center">
          <Col md={8} push={{md: 2}} className="primary-ph__preview">
            { detailEl }
          </Col>
        </Row>
      )
    }
  }

  render() {
    const { backgroundImage } = this.props
    const style = {
      backgroundImage: `url(${backgroundImage})`
    }

    return (
      <section style={ style } className={classnames('primary-ph', this.props.className)}>
        <Container className="primary-ph__content">
          { this.renderContent() }
        </Container>
      </section>
    )
  }
}

PrimaryPageHeader.propTypes = {
  title: PropTypes.string.isRequired,
  subtitle: PropTypes.string.isRequired,
  backgroundImage: PropTypes.string.isRequired,
  featuredImage: PropTypes.string,
  updated: PropTypes.string,
  prototypeId: PropTypes.string
  figmaLink: PropTypes.string
}

export default PrimaryPageHeader
