import React, { Component } from 'react'

class CheckList extends Component {
  render () {
    return (
      <ul className="check-list">
        {this.props.children}
      </ul>
    )
  }
}

export default CheckList;
