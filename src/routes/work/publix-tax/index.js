import React, { Component } from 'react'
import { Row, Col, Container } from 'react-grid-system'
import { connect } from 'react-redux'
import { withRouter } from "react-router";
import { bindActionCreators } from 'redux'
import * as actions from '../../../store/actions'
import OverviewPanel from '../../../Components/Panels/overview'
import Rating from '../../../Components/Rating'
import WorkLayout from '../../../layouts/work'
import PageHeader from '../../../Components/PageHeaders/Secondary'
import MasterDeck from '../../../decks/publix_tax_case-study_master.pdf'
import PublixAboutImage from '../../../images/publix-about.png'
import PublixPIMSImage from '../../../images/publix-pims-animation.gif'
import PublixProcessImage from '../../../images/publix-process.jpg'
import PublixTaxWorkshopImage from '../../../images/publix-workshop-1.jpg'
import PublixTaxEventStormImage from '../../../images/publix-event-storm.png'
import PublixTaxInterviewMVPImage from '../../../images/publix-tax-interview-mvp.png'
import LinkWithIcon from '../../../Components/Links/IconLabel'
import ColorBlock from '../../../Components/ContentBlock/ColorBlock'

function mapStateToProps(state) {
  return { page: state.pageList.productPage };
}

const mapDispatchToProps = (dispatch) => {
  const {addFeedback} = actions;
  return {
    addFeedback: bindActionCreators(addFeedback, dispatch)
  }
}

class PublixTax extends Component {
  constructor() {
    super();

    this.state = {
      rating: 0
    }
  }

  render() {
    let fullLink = `/password-gate?redirect=${MasterDeck}`

    return (
      <WorkLayout header="secondary" className="publix">
        <PageHeader
            title="Modernizing Publix's mainframe systems into a new, delightful experience"
            time="2022 - 2023 • Product Strategy | Facilitator | Acting BSA | Lead Designer"
            featuredImage={ PublixAboutImage }
            fullLink={fullLink}
            >
            <OverviewPanel>
              I led a large-scale, multi-year effort to redesign and modernize many of Publix's outdated internal systems. Overcoming growing pains and process gaps, we created a more efficient, scalable, and user-friendly solution, now used for thousands of items annually. Through our lean design approach, we were able to reduce the task completion time by 98%, improve auditing, saving Publix money on errors and storage cost.
            </OverviewPanel>
        </PageHeader>
        <Container>
          <Row className="work__block">
            <Col className="work__column" md={8} push={{ md: 2 }}>
              <header className="work-header">
                <h2>Publix has 1300+ locations in 8 states</h2>
              </header>
              <p>Publix has been recognized by <a href="https://www.theledger.com/story/business/2022/02/02/publix-ranks-no-1-among-grocers-customer-service-and-charity/9314040002/" target="_blank" rel="noopener noreferrer">Forbes as the #1 grocery chain</a>, employing over 240,000 people, generating over 48 billion in annual sales, and expanding operations across the southeastern region of the United States.</p>
            </Col>
          </Row>
          <Row className="work__block">
            <Col className="work__column" md={8} push={{md: 2}}>
              <header className="work-header">
                <h2>Bidding farewell to &#x27;90s mainframe, welcoming modern design</h2>
              </header>
              <p>With the imminent retirement of the developer, the only one who really knows this system inside out, Publix is set to invest significant resources into a new, state-of-the-art system designed to streamline business processes.</p>
              <img className="work__image" src={ PublixPIMSImage } alt="Current Mainframe System"/>
            </Col>
          </Row>
          <Row className="work__block">
            <Col className="work__column" md={8} push={{md:2}}>
              <header className="work-header">
                <h3>The current waterfall delivery culture combined with an average tenure of 25+ years made it difficult to be the champion for change.</h3>
              </header>
              <ul>
                <li><b>Current Process:</b> Waterfall</li>
                <li><b>Design Maturity:</b> Beginning stages</li>
                <li><b>Project State:</b> Previously created System Requirement Document (SRD).</li>
                <li><b>Teams:</b> Split into 5 newly formed agile teams</li>
                <li><b>My Assignment:</b> Discover new solutions through implementing user centered design processes</li>
              </ul>
            </Col>
          </Row>
          <Row className="work__block">
            <Col className="work__column">
              <ColorBlock title="What I'm Working With" color="pink" isDecor isCenter>
                <Col md={8} push={{md:2}} className="text--center">
                  <p>The current waterfall delivery culture combined with an average tenure of 25+ years made it difficult to be the champion for change.</p>
                </Col>
              </ColorBlock>
            </Col>
          </Row>
          <Row className="work__block">
            <Col className="work__column">
              <img className="work__image" src={ PublixProcessImage } alt="Discovery UX Process Diagram"/>
            </Col>
          </Row>
          <Row className="work__block">
            <Col className="work__column" md={8} push={{md:2}}>
              <header className="work-header">
                <h3>My strategy focused on starting to move past existing bias and opening up minds to possible alternate solutions through collaborative discovery.</h3>
              </header>
              <p>Generate assumptions -> Create tracker -> Create script -> Interview -> Hypothesize -> Repeat</p>
              <ul>
                <li><b>Prioritize learning over growth:</b> Make the right thing first then scale.</li>
                <li><b>Mitigate risk:</b> through small batch sizes and user-centered design.</li>
                <li><b>Empower the team:</b> Include the team throughout discovery process.</li>
                <li><b>Eliminate bias:</b> Breaking past pre-conceived solutions to discover new ideas.</li>
              </ul>
            </Col>
          </Row>
          <Row className="work__block">
            <Col className="work__column" md={8} push={{md: 2}}>
              <header className="work-header work__callout">
                <h2>We started with a holistic view of the current business processes</h2>
                <h3>Gathering everyone in one virtual room, we collaborated in an event storming session and came to an agreement on the current business processes during the lifecycle of an item.</h3>
              </header>
            </Col>
          </Row>
          <Row className="work__block">
            <Col className="work__column" md={6} push={{md: 3}}>
              <img className="work__image" src={PublixTaxEventStormImage} alt=""/>
            </Col>
          </Row>
          <Row className="work__block">
            <Col className="work__column" md={8} push={{md: 2}}>
              <header className="work-header work__callout">
                <h2>01. Discovering our MVP</h2>
                <h3>The new Publix Item System is your daily companion tax application accessible from your desktop. Choose an item in the queue, determine tax plan, and you're ready to apply the tax how you normally do today.</h3>
              </header>
              <ul className="work__results">
                <li><span><b>80%</b>↑</span>task time efficiency</li>
                <li><span><b>10k</b>+</span>new items per year</li>
                <li><span><b>40%</b>↓</span>steps in process flow</li>
              </ul>
            </Col>
          </Row>
          <Row className="work__block">
            <Col className="work__column">
              <img className="work__image" src={ PublixTaxInterviewMVPImage } alt="Discovery UX Process Diagram"/>
            </Col>
          </Row>
          <Row className="work__block">
            <Col className="work__column" md={8} push={{md: 2}}>
              <header className="work-header work__callout">
                <h2>02. Eliminate current system</h2>
                <h3>Phase 2 addressed the limitations of the existing item grouping system, which, while efficient for Buyers, hindered accurate taxation. The solution involved a separate, tax-oriented grouping system.</h3>
              </header>
              <ul className="work__results">
                <li><span><b>98%</b>↑</span>task time efficiency</li>
                <li><span><b>~80%</b>↓</span>reduced paper waste</li>
                <li><span><b>100%</b>↓</span>savings on tax errors</li>
              </ul>
            </Col>
          </Row>
          <Row className="work__block">
            <Col className="work__column">
              <img className="work__image" src={ PublixTaxWorkshopImage } alt="Discovery UX Process Diagram"/>
            </Col>
          </Row>
          <Row className="work__block">
            <Col className="work__column" md={8} push={{md: 2}}>
              <header className="work-header work__callout">
                <h2>03. Create Tax Events</h2>
                <h3>Phase 3 tackled the financial costs arising from inaccurate tax collection during tax-free events by empowering users with the ability to create highly tailored tax-free scenarios.</h3>
              </header>
              <ul className="work__results">
                <li><span><b>1</b>↓</span>system of reference</li>
                <li><span><b>60%</b>↓</span>steps in process flow</li>
                <li><span><b>100%</b>↓</span>reduced paper waste</li>
              </ul>
            </Col>
          </Row>
          <Row className="work__block">
            <Col className="work__column" md={8} push={{md:2}}>
              <header className="work-header">
                <h2>Stakeholders almost went in a different direction</h2>
              </header>
              <p>Though I successfully united the teams around innovative solutions, past SRD commitments resurfaced, pushing discovery to the sidelines and prioritizing delivery-centric roadmaps. Thankfully, the team was fully onboard and continued to champion our solution, ultimately allowing our users to win in the end.</p>
            </Col>
          </Row>
          <Row className="work__block">
            <Col className="work__column" md={8} push={{md: 2}}>
              <header className="work-header work__callout">
                <h2>Takeaways</h2>
                <LinkWithIcon className="work__link" to={ fullLink } icon="lock">See Full Case Study</LinkWithIcon>
              </header>
              <p>I'm proud to say we drastically improved our users daily workflow by significantly streamlining the process, eliminating errors, and most importantly showcased the value discovery research brings to the business.</p>
            </Col>
          </Row>
          <Row className="work__block work__conclusion">
            <Col xs={8} push={{xs: 2}}>
              <Rating {...this.props} />
            </Col>
          </Row>
        </Container>
      </WorkLayout>
    );
  }
}

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps)(PublixTax)
);
